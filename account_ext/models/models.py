# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
import webbrowser
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import json
import urllib2
from datetime import datetime
import requests
from requests.exceptions import ConnectionError
#

# class account_inherit(models.Model):
#     inherit = 'account.invoice'
#     pan_num = fields.Char(string='Pan number')

    # data mapping ###
    # left side sales.registerko field
    # @api.multi
    # def create_register(self):
    #     inv_obj = self.env['sales.register']
    #     self.ensure_one()
    #     slip = inv_obj.create({
    #         'name': self.partner_id,
    #         'invoice_date': self.date_invoice,
    #         'invoice_number': self.name,
    #         # '': self.name,
    #     })
    #     return slip
    #
    # def create_register_btn(self):
    #     self._create_register()



class account_ext(models.Model):
    _name='account.ase'

    def open1(self):
        webbrowser.open_new(r'account_ext/models/user_manual_accounting.pdf')
    #
    # def generate_xml(self, cr, uid, ids, context=None):
    #     xml_data = text_to_write_in_xml
    #     file = base64.encodestring(xml_data)
    #     self.write(cr, uid, ids, {'filedata': file, 'name': "file.xml"}, context=context)
    #     return {
    #         'type': 'ir.actions.act_window',
    #         'res_model': 'my.class',
    #         'view_mode': 'form',
    #         'view_type': 'form',
    #         'target': ids,
    #         'context': dict(context, active_ids=ids)
    #         'views': [(False, 'form')],
    #         'target': 'new',
    #     }

class invoice_inherit(models.Model):
    _inherit = 'account.invoice'
    untaxed_amount = fields.Float("Total Untaxed", compute="_compute_untaxed", store=True, readonly=True)
    # is_realtime = fields.Boolean(string = 'Is Realtime?')
    # sync_with_ird = fields.Boolean(string = 'Sync with IRD?')

    @api.multi
    def action_invoice_open(self):

        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
        if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2', 'draft']):
            raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
        to_open_invoices.action_date_assign()
        to_open_invoices.action_move_create()
        # raise ValidationError('done!')
        # self.test_selection()
        self.call_server_api()
        # import pdb;
        # pdb
        # self._sms_trigger_invoice()
        return to_open_invoices.invoice_validate()


    def test_selection(self):
        key_val = dict(self._fields['fiscal_year'].selection).get(self.fiscal_year)
        raise ValidationError(key_val)

    @api.depends('amount_untaxed')
    def _compute_untaxed(self):
        for line in self:
            line.untaxed_amount += line.amount_untaxed

    # data flow to the new class for return purchase register if it's a vender bill
    def purchase_return(self):
        # sale_obj = self.env['sale.return.register']
        zo = self.env['account.invoice']
        pur_obj = self.env['purchase.return.register']
        for z in zo:
            if z.journal_id.type == 'purchase':
                inv = pur_obj.create({
                    'date_invoice': z.date_invoice,
                    'number': z.number,
                    'partner_id': z.partner_id,
                    'pan_num': z.pan_num,
                    'amount_total': z.amount_total,
                    'untaxable_amount': z.untaxable_amount,
                    'taxable_amount_value': z.taxable_amount_value,
                    'taxable_amount_tax': z.taxable_amount_tax,
                    'taxable_import_value': z.taxable_import_value,
                    'taxable_import_tax': z.taxable_import_tax,
                    'taxable_capital_purchase_value': z.taxable_capital_purchase_value,
                    'taxable_capital_purchase_tax': z.taxable_capital_purchase_tax
                })
                return inv

    # data flow to the new class for return sale register if it's a customer invoice
    def sale_return(self):
        # raise ValidationError(' Refund called')
        # sale_obj = self.env['sale.return.register']
        sale_obj = self.env['sale.return.register']
        print sale_obj
        # raise ValidationError('sale refund')
        for z in self:
            if z.journal_id.type == 'sale':
                inv = sale_obj.create({
                    'date_invoice': z.date_invoice,
                    'number': z.number,
                    'partner_id': z.partner_id,
                    'pan_num': z.pan_num,
                    'amount_total': z.amount_total,
                    'untaxable_amount': z.untaxable_amount,
                    'zero_rated_sales': z.zero_rated_sales,
                    'taxable_amount': z.taxable_amount,
                    'amount_tax': z.amount_tax

                })
                return inv

    @api.multi
    def invoice_refund(self):
        data_refund = self.read(['filter_refund'])[0]['filter_refund']
        self.purchase_return()
        return self.compute_refund(data_refund)

    @api.multi
    def call_server_api(self):
        # data = {
        #     'username': "Test_CBMS",
        #     'password': "test@321",
        #     'seller_pan': "9999",
        #     'buyer_pan': self.partner_id.pan_number,
        #     'buyer_name': self.partner_id.name,
        #     'fiscal_year': self.fiscal_year,
        #     'invoice_number': self.number,
        #     'invoice_date': self.date_invoice,
        #     'total_sales': 0,
        #     'taxable_sales_vat': 1000.01,
        #     'vat': 130,
        #     'excisable_amount': 0,
        #     'excise': 0,
        #     'taxable_sales_hst': 0,
        #     'hst': 0,
        #     'amount_for_esf': 0,
        #     'esf': 0,
        #     'esport_sales': 0,
        #     'tax_exempted_sales': 0,
        #     'isrealtime': True,
        #     'datetimeclient': datetime.now().strftime('%Y-%m-%d')
        #
        #
        # }

        # inv_date = self.date_invoice
        # inv_date.strftime('%Y.%m.%d')



        if self.origin:
            data_ird_refund = {
                "username": "Test_CBMS",
                "password": "test@321",
                "seller_pan": "999999999",
                "buyer_pan": "123456789",
                "buyer_name": self.partner_id.name,
                "fiscal_year": self.fiscal_year,
                "ref_invoice_number": self.origin,
                "credit_note_number": self.number,
                "credit_note_date": self.date_invoice,
                "reason_for_return": self.name,
                "total_sales": self.amount_total,
                "taxable_sales_vat": self.amount_exc,
                "vat": self.amount_tax,
                "excisable_amount": self.amount_untaxed,
                "excise": self.amount_excise,
                "taxable_sales_hst": 0,
                "hst": 0,
                "amount_for_esf": 0,
                "esf": 0,
                "export_sales": 0,
                "tax_exempted_sales": self.untaxed_amount,
                "isrealtime": True,
                "datetimeclient": str(datetime.now().strftime('%Y.%m.%d'))
            }

            try:
                headers = {'Content-type': 'application/json'}
                r = requests.post('http://202.166.207.75:9050/api/billreturn', data=json.dumps(data_ird_refund),
                                  headers=headers)
                response_code = r.json()
                if response_code == 200 or response_code == 101:
                    self.sync_with_ird = True
                    # raise ValidationError(response_code)
                elif response_code == 100:
                    self.sync_with_ird = False
                    # raise ValidationError("api credential does not match", self.sync_with_ird)
                else:
                    # raise ValidationError("Else thing")
                    self.sync_with_ird = False
            except ConnectionError as e:
                print e
                raise UserError("Data will be save later in IRD Server")

            # headers = {'Content-type': 'application/json'}
            # r = requests.post('http://202.166.207.75:9050/api/billreturn', data=json.dumps(data_ird_refund), headers=headers)
            # response_code = r.json()



        else:
            data_ird = {
                "username": "Test_CBMS",
                "password": "test@321",
                "seller_pan": "999999999",
                "buyer_pan": "123456789",
                "buyer_name": self.partner_id.name,
                "fiscal_year": self.fiscal_year,
                "invoice_number": self.number,
                "invoice_date": self.date_invoice,
                "total_sales": self.amount_total,
                "taxable_sales_vat": self.amount_untaxed,
                "vat": self.amount_tax,
                "excisable_amount": self.amount_untaxed,
                "excise": self.amount_excise,
                "taxable_sales_hst": 0,
                "hst": 0,
                "amount_for_esf": 0,
                "esf": 0,
                "export_sales": 0,
                "tax_exempted_sales": self.untaxed_amount,
                "isrealtime": True,
                "datetimeclient": str(datetime.now().strftime('%Y.%m.%d'))
            }

            try:
                headers = {'Content-type': 'application/json'}
                r = requests.post('http://202.166.207.75:9050/api/bill', data=json.dumps(data_ird), headers=headers)
                response_code = r.json()

                if response_code == 200 or response_code == 101:
                    self.sync_with_ird = True
                    self.is_realtime = True
                    # raise ValidationError(response_code)
                elif response_code == 100:
                    self.sync_with_ird = False
                    raise ValidationError(self.sync_with_ird)
                else:
                    self.sync_with_ird = False
                    # raise ValidationError("Else thing")
            except ConnectionError as e:
                print e
                # raise UserError("Data will be save later in IRD Server")


                    # raise ValidationError(data_ird)

        # raise ValidationError(inv_date)


        # date conversion test
        # datetimeclient= datetime.now().strftime('%Y.%m')
        # raise ValidationError(datetimeclient)
        # headers = {'Content-type': 'application/json'}
        # r = requests.post('http://202.166.207.75:9050/api/bill', data=json.dumps(data_ird), headers=headers)
        # raise ValidationError(r.json())


    # schedular, to check sync_with_ird overtime

    def ird_post_scheduler_queue(self):
        scheduler_line_obj = self.env['account.invoice'].search([['sync_with_ird', '=', False]])

        if scheduler_line_obj != None:
            for rec in scheduler_line_obj:
                # if self.number:
                if self.origin:
                    data_ird_refund = {
                        "username": "TestBMS",
                        "password": "test@321",
                        "seller_pan": "999999999",
                        "buyer_pan": "123456789",
                        "buyer_name": rec.partner_id.name,
                        "fiscal_year": rec.fiscal_year,
                        "ref_invoice_number": rec.origin,
                        "credit_note_number": rec.number,
                        "credit_note_date": rec.date_invoice,
                        "reason_for_return": rec.name,
                        "total_sales": rec.amount_total,
                        "taxable_sales_vat": rec.amount_exc,
                        "vat": rec.amount_tax,
                        "excisable_amount": rec.amount_untaxed,
                        "excise": rec.amount_excise,
                        "taxable_sales_hst": 0,
                        "hst": 0,
                        "amount_for_esf": 0,
                        "esf": 0,
                        "export_sales": 0,
                        "tax_exempted_sales": rec.untaxed_amount,
                        "is_realtime": False,
                        "datetimeclient": str(datetime.now().strftime('%Y.%m.%d'))
                    }

                    try:
                        headers = {'Content-type': 'application/json'}
                        r = requests.post('http://202.166.207.75:9050/api/billreturn', data=json.dumps(data_ird_refund),
                                          headers=headers)
                        response_code = r.json()

                        if response_code == 200 or response_code == 101:
                            rec.sync_with_ird = True
                            # raise ValidationError(response_code)
                        elif response_code == 100:
                            rec.sync_with_ird = False
                            # raise ValidationError("api credential does not match", rec.sync_with_ird)
                        else:
                            # raise ValidationError("Else thing")
                            rec.sync_with_ird = False

                    except ConnectionError as e:
                        print e

                else:
                    data_ird = {
                        "username": "Test_CBMS",
                        "password": "test@321",
                        "seller_pan": "999999999",
                        "buyer_pan": "123456789",
                        "buyer_name": rec.partner_id.name,
                        "fiscal_year": rec.fiscal_year,
                        "invoice_number": rec.number,
                        "invoice_date": rec.date_invoice,
                        "total_sales": rec.amount_total,
                        "taxable_sales_vat": rec.amount_exc,
                        "vat": rec.amount_tax,
                        "excisable_amount": rec.amount_untaxed,
                        "excise": rec.amount_excise,
                        "taxable_sales_hst": 0,
                        "hst": 0,
                        "amount_for_esf": 0,
                        "esf": 0,
                        "export_sales": 0,
                        "tax_exempted_sales": rec.untaxed_amount,
                        "isrealtime": False,
                        "datetimeclient": str(datetime.now().strftime('%Y.%m.%d'))
                    }

                    try:
                        headers = {'Content-type': 'application/json'}
                        r = requests.post('http://202.166.207.75:9050/api/bill', data=json.dumps(data_ird), headers=headers)
                        response_code = r.json()

                        if response_code == 200 or response_code == 101:
                            rec.sync_with_ird = True
                            # raise ValidationError(response_code)
                        elif response_code == 100:
                            rec.sync_with_ird = False
                            # raise ValidationError(rec.sync_with_ird)
                        else:
                            rec.sync_with_ird = False
                            # raise ValidationError("Else thing")
                    except ConnectionError as e:
                        print e


class account_invoice_refund_inherit(models.TransientModel):
    _inherit = 'account.invoice.refund'

    @api.multi
    def invoice_refund(self):
        # import pdb;
        # pdb.set_trace()
        # self.sale_return()
        # self.purchase_return()
        data_refund = self.read(['filter_refund'])[0]['filter_refund']
        self.call_method()
        # self.env['account.invoice'].purchase_return()
        # self.env['account.invoice'].sale_return()
        # invoice_inherit.sale_return()
        # invoice_inherit.purchase_return()

        return self.compute_refund(data_refund)

    # data flow to the new class for return purchase register if it's a vender bill
    @api.multi
    def purchase_return(self):
        # sale_obj = self.env['sale.return.register']
        print "purchase return"
        zo = self.env['account.invoice']
        pur_obj = self.env['purchase.return.register']
        for z in zo:
            if z.journal_id.type == 'purchase':
                inv = pur_obj.create({
                    'date_invoice': z.date_invoice,
                    'number': z.number,
                    'partner_id': z.partner_id,
                    'pan_num': z.pan_num,
                    'amount_total': z.amount_total,
                    'untaxable_amount': z.untaxable_amount,
                    'taxable_amount_value': z.taxable_amount_value,
                    'taxable_amount_tax': z.taxable_amount_tax,
                    'taxable_import_value': z.taxable_import_value,
                    'taxable_import_tax': z.taxable_import_tax,
                    'taxable_capital_purchase_value': z.taxable_capital_purchase_value,
                    'taxable_capital_purchase_tax': z.taxable_capital_purchase_tax
                })
                return inv


                # data flow to the new class for return sale register if it's a customer invoice

    @api.multi
    def sale_return(self):
        print "sale return"
        # raise ValidationError(' Refund called')
        # sale_obj = self.env['sale.return.register']
        so = self.env['account.invoice']
        sale_obj = self.env['sale.return.register']
        for z in so:
            if z.journal_id.type == 'sale':
                inv = sale_obj.create({
                    'date_invoice': z.date_invoice,
                    'number': z.number,
                    'partner_id': z.partner_id,
                    'pan_num': z.pan_num,
                    'amount_total': z.amount_total,
                    'untaxable_amount': z.untaxable_amount,
                    'zero_rated_sales': z.zero_rated_sales,
                    'taxable_amount': z.taxable_amount,
                    'amount_tax': z.amount_tax

                })
                return inv

    def call_method(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            inv = self.env['account.invoice'].browse(active_id)
            inv.write({'active_in': 'inactive'})

class sales_return_register(models.Model):
    _name='sale.return.register'
    date_invoice=fields.Char(string='Date')
    number=fields.Char(string='Invoice Number')
    partner_id=fields.Char(string="Purchaser Name")
    pan_num=fields.Char(string='Purchaser Pan')
    amount_total=fields.Char(string='Total Sale')
    untaxable_amount=fields.Char(string="Tax Exempted Sales")
    zero_rated_sales=fields.Char(string='Zero Rated Sales')
    taxable_amount=fields.Char(string = "Taxable Sales Value" )
    amount_tax=fields.Char(string = "Taxable Sales Tax")

class purchase_return_register(models.Model):
    _inherit = 'account.invoice.refund'
    _name='purchase.return.register'
    date_invoice=fields.Char(string='Date')
    number=fields.Char(string='Invoice No')
    partner_id=fields.Char(string="Supplier Name")
    pan_num=fields.Char(string='Supplier Pan')
    amount_total=fields.Char(string='Total Purchase')

    untaxable_amount=fields.Char(string="Tax Exempted Purchase")

    taxable_amount_value=fields.Char(string = "Taxable Purchase Value" )
    taxable_amount_tax=fields.Char(string = "Taxable Purcahse Tax")
    taxable_import_value=fields.Char(string = "Taxable Import Value")
    taxable_import_tax=fields.Char(string = "Taxable Import Tax")
    taxable_capital_purchase_value=fields.Char(string = "Taxable Capital Purchase Value" )
    taxable_capital_purchase_tax=fields.Char(string='Total Capital Purchase Tax')




            # pan_number = fields.Char(string='PAN Number',related='partner_id.pan_number')

# class sales_register(models.Model):
#     _name = 'sales.register'
#     _inherit = ['account.invoice']
#     # account_id = fields.Many2one('account.invoice')
#
#     # invoice_date = fields.Date(string='Invoice Date')
#     # name = fields.Many2one('res.partner',string='Customer')
#     pan_number = fields.Char(string='PAN Number')
#     # data mapping ###
    # left side sales.registerko field
    # @api.multi
    # def create_register(self):
    #     inv_obj = self.env['sales.register']
    #     self.ensure_one()
    #     slip = inv_obj.create({
    #         'name': self.partner_id,
    #         'invoice_date': self.date_invoice,
    #         'invoice_number': self.name,
    #         # '': self.name,
    #     })
    #     return slip
    #
    # def create_register_btn(self):
    #     self._create_register()

    # invoice_number = fields.Char(sting='Invoice number')
    # total = fields.Char(string='Total sales')
    # amt_without_vat = fields.Char(string='amount without tax')
    # unit_price = fields.Char(string='Unit Price')


# class purchases_register(models.Model):
#     _name = 'purchases.register'
#     _inherit = ['account.invoice']

# class reg_payment(models.Model):
#     # _name = 'reg.payment'
#     _inherit = 'account.payment'

#     def post(self):
#         print "printed"
        # raise ValidationError("To be validated")
