# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import re
import subprocess
from odoo.tools import amount_to_text_en, float_round
import math
import inflect


class Print(models.Model):
    _inherit = 'account.invoice'

    print_count = fields.Integer("Print Count", default=lambda x: _(0))
    # is_printed = fields.Char("Is Printed?", default='false')
    sync_with_ird = fields.Boolean(default=False)
    is_realtime = fields.Boolean(default=False)
    extract_month = fields.Char(string='Month', compute="extract_date")
    extract_year = fields.Char(string='Year', compute="extract_date", store=True)

    @api.onchange('date_invoice')
    def extract_date(self):
        #inv_obj = self.env['account.invoice']
	for d in self:
		date = datetime.strptime(d.date_invoice, "%Y-%m-%d").date()
		# d = date_invoice
		tdate = date.month
		# month = date.month
		ydate=date.year
		self.extract_year = ydate
		
		
	
		if tdate==1:
		    month = "January"

		elif (tdate==2):
		    month= "Feburary"

		elif (tdate==3):
		    month= "March"

		elif (tdate==4):
		    month= "April"

		elif (tdate==5):
		    month= "May"

		elif (tdate==6):
		    month= "June"

		elif (tdate==7):
		    month= "July"

		elif (tdate==8):
		    month= "August"

		elif (tdate==9):
		    month= "September"

		elif (tdate==10):
		    month= "October"

		elif (tdate==11):
		    month= "November"

		elif (tdate==12):
		    month= "December"


		self.extract_month=month	

    

#amount in words

    check_number_in_words = fields.Char(string="Amount in Words")


    def _get_check_number_in_words(self,amount_total):
        # check_number_in_words = amount_to_text_en.amount_to_text(math.floor(amount_total), lang='en', currency='')
        # check_number_in_words = check_number_in_words.replace(' and Zero Cent', '')
        # decimals = amount_total % 1
        # if decimals >= 10**-2:
        # raise ValidationError(decimals)
        # decimal_amount=amount_to_text_en.amount_to_text(math.floor(decimals), lang='en', currency='')
        # raise ValidationError(decimal_amount)
        # # check_number_in_words += _(' and %s/100') % str(int(round(float_round(decimals*100, precision_rounding=1))))
        # check_number_in_words += decimal_amount

        p = inflect.engine()
        check_number_in_words = p.number_to_words(amount_total)
        check_number_in_words = check_number_in_words.title()

        return check_number_in_words

    # def _get_check_number_in_words(self,amount_total):
    #     check_number_in_words = amount_to_text_en.amount_to_text(math.floor(amount_total), lang='en', currency='')
    #     check_number_in_words = check_number_in_words.replace(' and Zero Cent', '')
    #     decimals = amount_total % 1
    #     if decimals >= 10**-2:
    #         check_number_in_words += _(' and %s/100') % str(int(round(float_round(decimals*100, precision_rounding=1))))
    #     return check_number_in_words

    @api.onchange('amount_total')
    def _onchange_amount(self):
        if hasattr(super(Print, self), '_onchange_amount'):
            super(Print, self)._onchange_amount()
        self.check_number_in_words = self._get_check_number_in_words(self.amount_total)





    # @api.multi()
    # @api.depends('print_count')
    # def create_report(self):
    #     self.onchange_count()
    #     self.printed()    
    #     # raise ValidationError("Print statement")
    #     return self.env['report'].get_action(self, 'account_report_ext.report_invoice_final_template')
        
    
    is_printed = fields.Char("Is Printed?", default="False")
    #
    # @api.model
    # @api.onchange('print_count')
    # def printed(self):
    #     if self.print_count != 0:
    #         printed = self.search([('is_printed', '=', False)])
    #         printed.write({'is_printed': True})
    #         return True

    active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")

    printed_by = fields.Char("Printed By", store=True)
    printed_time = fields.Datetime("Printed Time", default=fields.Datetime.now())
    
    @api.multi
    def create_mtrl_report(self):
        self._default_name()
        self._default_time()
        self.call_server_api()
        # self.filtered(lambda s: s.state == 'draft').write({'state': 'sent'})
        return self.env['report'].get_action(self, 'account_report_ext.report_material_template')

    @api.multi
    @api.depends('printed_by')
    def _default_name(self):
        print_obj = self.env.user.name
        self.printed_by = print_obj
        return True

    @api.multi
    @api.depends('printed_time')
    def _default_time(self):
        now = datetime.now()
        self.printed_time = now
        return True

    # class MV(models.Model):
    #     _name = "invoice.mv"
        
    #     _inherit = ['account.invoice']
        

        
        # _inherits = {'account.invoice': 'name'}
        
        

        # apple = fields.Char("Diwakar", size=20)
    # assets ################################################################

    taxable_capital_purchase_value = fields.Float(string="Taxable Capital Purchase Value")
    # taxable_capital_purchase_value = fields.Float(string="Taxable Capital Purchase Value", compute='check_assests')
    # product_amount1_non_asset = fields.Float(string="Product Service", compute='check_assests')

    @api.multi
    def check_assests(self):

        for r in self:
            for rec in r.invoice_line_ids:
                # import pdb;
                # pdb.set_trace()
                if (rec.product_id.type == 'asset' and self.amount_tax == 0):
                    r.taxable_capital_purchase_value = r.amount_total-r.amount_tax
                    r.taxable_capital_purchase_tax = r.amount_tax
                    r.taxable_amount = None
                    r.taxable_amount_value = None
                    r.taxable_import_value = None
                    r.untaxable_amount = self.amount_total
                    # r.taxable_amount_value = None
                    r.taxable_amount_tax = None
                    # needs to be seriously changed
                    # r.amount_untaxed = None
                    # r.taxable_amount_value = None
                    # raise ValidationError(r.amount_untaxed)

                elif (rec.product_id.type == 'asset'):
                    # self.product_amount = self.product_amount + self.invoice_line_ids.price_subtotal
                    r.taxable_capital_purchase_value = r.amount_untaxed
                    r.taxable_capital_purchase_tax = r.amount_tax
                    r.taxable_amount = None
                    r.taxable_amount_value = None
                    r.taxable_import_value = None
                    r.taxable_import_tax = None
                    r.taxable_amount_value = None

                # else:
                #     rec.product_amount1 = rec.amount_untaxed
                #     rec.product_amount1 = sum(line.price_subtotal for line in rec.invoice_line_ids)


            # else:


class ProductTemplateInherit(models.Model):
    _inherit = "product.template"
    type = fields.Selection([
        ('asset', _('Asset')),
        ('consu', _('Consumable')),
        ('service', _('Service'))], string='Product Type', default='consu', required=True,
        help='A stockable product is a product for which you manage stock. The "Inventory" app has to be installed.\n'
             'A consumable product, on the other hand, is a product for which stock is not managed.\n'
             'A service is a non-material product you provide.\n'
             'A digital content is a non-material product you sell online. The files attached to the products are the one that are sold on '
             'the e-commerce such as e-books, music, pictures,... The "Digital Product" module has to be installed.')

class PrintPreview(models.TransientModel):
    _name="account.invoice.print.preview"

    name = fields.Char("Print Document Name")

    @api.multi
    def print_report(self):
        return self.env['report'].get_action(self, 'account_report_ext.account_invoice_report_reprint_main')
        # print_rp = self.env['report'].get_action(self, 'account_report_ext.account_invoice_report_reprint_main')
        # window.print(print_rp)
        # return print_rp
