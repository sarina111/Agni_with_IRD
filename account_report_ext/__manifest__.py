# -*- coding: utf-8 -*-
{
    'name': "Account Report Ext",

    'summary': """
        Reprint Numbering""",

    'description': """
        Account invoice can be printed multiple times but with title showing Reprint and iterated number.
    """,

    'author': "BI Solutions",
    'website': "http://www.bisolutions.asia",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'applications',
    'version': '1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'report_xml','sales_ext_invoice','account_accountant'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
	'material_view_report.xml',
	'sale_register.xml',
	'pur_reg.xml',
	'invoice_agni.xml',
	'purchase_return_register.xml',
	'sales_return.xml',
	'xml_salesreg.xml',
	'xml_salesreturn.xml',
	'xml_pur.xml',
	'xml_pur_return.xml',
	'xml_materializedview.xml'
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}
